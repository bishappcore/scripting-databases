CREATE TABLE active_customers (
buyer_name VARCHAR(100),
sales_price NUMERIC(8,2),
cut_quality INT,
purchase_date DATE
);